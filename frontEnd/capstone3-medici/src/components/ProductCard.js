import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
export default function ProductCard({product}) {

	const {productName, description, price, _id} = product;

	return (
		<Card>
			<Card.Body>
				<Card.Title>{productName}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>Php {price}</Card.Text>
				<Button className="bg-primary" as={Link} to={`/products/details/${_id}`}>Details</Button>
			</Card.Body>
		</Card>
	)
}
import {Table, Container} from 'react-bootstrap';

export default function AllUsers({user}) {
	
	const {fullName, email, mobileNo, isAdmin} = user;

	let isRole = isAdmin;
	function role (isRole) {
		if(isRole) {
			return "Admin"
		} else {
			return "User"
		}
	};

	return (
		<>
		<Container>
			<h2>Manage Users</h2>
			<Table striped bordered hover variant="dark" className="col-9">
				<thead>
					<tr>
						<th>Full Name</th>
						<th>Email</th>
						<th>Mobile No</th>
						<th>Role</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>{fullName}</td>
						<td>{email}</td>
						<td>{mobileNo}</td>
						<td>{role(isRole)}</td>
					</tr>
				</tbody>
			</Table>
		</Container>
		</>
	)
};
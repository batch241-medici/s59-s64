import {useState} from 'react';
import {Carousel, Container} from 'react-bootstrap';

export default function Banner({data}) {

	const {title, description} = data;

	const [index, setIndex] = useState(0)

  	const handleSelect = (selectedIndex, e) => {
    	setIndex(selectedIndex)
	}

	return (
		<>
		<Container>
			<Carousel activeIndex={index} onSelect={handleSelect} id="carousel">
	      		<Carousel.Item>
			        <img
			          className="d-block w-100"
			          src="https://image.cnbcfm.com/api/v1/image/106811484-1608045351058-gettyimages-1126750618-dsc_1540.jpeg?v=1608045431"
			          alt="First slide"
			          height="550"
			        />
			        <Carousel.Caption>
			          <h3>{title}</h3>
			          <p>{description}</p>
			        </Carousel.Caption>
	      		</Carousel.Item>
	      		<Carousel.Item>
			        <img
			          className="d-block w-100"
			          src="https://nmccat.com/wp-content/uploads/2020/09/Cat-Backhoe-1.jpg"
			          alt="First slide"
			          height="550"
			        />
			        <Carousel.Caption>
			          <h3>{title}</h3>
			          <p>{description}</p>
			        </Carousel.Caption>
	      		</Carousel.Item>
	    	</Carousel>
	   </Container>
	   </>
	)
}
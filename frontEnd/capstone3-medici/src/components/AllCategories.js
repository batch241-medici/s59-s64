import {Table, Container} from 'react-bootstrap';
import {useContext, useState, useEffect} from 'react';

import UserContext from '../UserContext';

export default function AllCategories() {
	const {category} = useContext(UserContext);

	const [categoryName, setCategoryName] = useState('');
	const [isActive, setIsActive] = useState(true);


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/categories/allCategories`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setCategoryName(data.categoryName);
			setIsActive(data.isActive);
		});
	}, []);

	return (
		<>
			<h2>Manage Categories</h2>
			<Table striped bordered hover variant="dark" className="col-9">
				<thead>
					<tr>
						<th>Category Name</th>
						<th>Active</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>{categoryName}</td>
						<td>{isActive}</td>
					</tr>
				</tbody>
			</Table>
		</>
	)
}
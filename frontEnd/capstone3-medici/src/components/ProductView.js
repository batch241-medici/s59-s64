import {useState, useEffect, useContext} from 'react';
import {Container, Card, Button, Row, Col} from 'react-bootstrap';
import {useParams, Link, useNavigate} from 'react-router-dom';

import UserContext from '../UserContext';
import Swal from 'sweetalert2';

export default function ProductView() {

	const {user} = useContext(UserContext);

	const {productId} = useParams();

	const navigate = useNavigate();

	const [productName, setProductName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [quantity, setQuantity] = useState(0);
	const [number, setNumber] = useState(0);
	const [isActive, setIsActive] = useState(false);

	function increment () {
		setNumber(number + 1);
	}

	function decrement() {
		setNumber(number - 1);
	}

	const checkout = (productId) => {
		let quantity = number;
		fetch(`${process.env.REACT_APP_API_URL}/products/orders/checkout`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true) {
				Swal.fire({
					title: "Checkout successful",
					icon: "success",
					text: "Product checkout is successful"
				})
				navigate("/catalog");
			} else {
				Swal.fire({
					title: "Erron on checkout",
					icon: "error",
					text: "Try again later"
				})
			}
		})
	}

	useEffect( () => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setProductName(data.productName);
			setDescription(data.description);
			setPrice(data.price);
			setQuantity(data.quantity);
		});
		if(number === quantity) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}

	}, [productId, number, quantity])

	return (
		(user.isAdmin === true) ?
		<>
			<Container>
				<Row>
					<Col lg={{span: 6, offset:3}} >
						<Card>
							<Card.Body className="text-center">
						    	<Card.Title>{productName}</Card.Title>
						        <Card.Subtitle>Description:</Card.Subtitle>
						        <Card.Text>{description}</Card.Text>
						        <Card.Subtitle>Price:</Card.Subtitle>
						        <Card.Text>PhP {price}</Card.Text>
						        <Card.Subtitle>Stocks:</Card.Subtitle>
						        <Card.Text>{quantity}</Card.Text>
						        
						        <Button variant="secondary" onClick={() => checkout(productId)} disabled>Checkout</Button>
						        <Button className="primary" as={Link} to="/catalog">Catalog</Button>
						      </Card.Body>

						</Card>
						
					</Col>
				</Row>
			</Container>
		</>
		:
		<>
		<Container>
			<Row>
				<Col lg={{span: 6, offset:3}} >
					<Card>
					      <Card.Body className="text-center">
					        <Card.Title>{productName}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>
					        <Card.Subtitle>Stocks:</Card.Subtitle>
					        <Card.Text>{quantity}</Card.Text>
					        {isActive?
					        <div className="d-md-flex justify-content-center">
					        <Button variant="secondary" id="decrement" onClick={() => decrement()}>-</Button>
					        <h2>{number}</h2>
					        <Button variant="secondary" id="increment" onClick={() => increment()} disabled>+</Button>
					        </div>
					        :
					        <div className="d-md-flex justify-content-center">
					        <Button variant="secondary" id="decrement" onClick={() => decrement()}>-</Button>
					        <h2>{number}</h2>
					        <Button variant="secondary" id="increment" onClick={() => increment()}>+</Button>
					        </div>
					    	}
					        
					        {(user.id !== null) ?
					        <Button variant="primary" onClick={() => checkout(productId)} >Checkout</Button>
					        :
					        <Button variant="danger" as={Link} to="/login"  >Log in to checkout</Button>
					       	}
					       	<Button className="primary" as={Link} to="/catalog">Catalog</Button>
					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
		</>

	)
}
import {useState, useEffect} from 'react';
import {Table, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function AllProduct({product}) {

	let { productName, description, isActive, price, quantity, _id} = product;

	const [isProductActive, setIsProductActive] = useState(isActive);
	const [deactivate, setDeactivate] = useState(false);
	const [activate, setActivate] = useState(true);

	let isActiveProduct = isActive;
	function active(isActiveProduct) {
		if(isActiveProduct) {
			return "Active"
		} else {
			return "Inactive"
		}
	};

	function Activate(_id) {
		let isActive = true;
		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`, {
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

		})
	}

	function Deactivate(_id) {
		let isActive = false;
		fetch(`${process.env.REACT_APP_API_URL}/products/${_id}`, {
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				isActive: isActive
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

		})
	}

	useEffect(() => {
		if(isActive) {
			setIsProductActive(false)
		} else {
			setIsProductActive(true)
		}
	}, [isActive])

	return (
		<>
			<h2>Manage Products</h2>
			<Table striped bordered hover variant="dark" className="col-9">
				<thead>
					<tr>
						<th>Product Name</th>
						<th>Description</th>
						<th>Status</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>{productName}</td>
						<td>{description}</td>
						<td>{active(isActiveProduct)}</td>
						<td>{price}</td>
						<td>{quantity}</td>
						<td>
							<Button variant="success" as={Link} to={`/products/${_id}`}>
							Update
							</Button>
							{ isProductActive ?
							<Button variant="primary" onClick={() => Activate(_id)}>
							Activate
							</Button>
							:
							<Button variant="danger" onClick={() => Deactivate(_id)}>
							Deactivate
							</Button>
							}
							
						</td>
					</tr>
				</tbody>
			</Table>
			<Button variant="primary" type="submit" id="submitBtn" as={Link} to="/dashboard">
				Dashboard
			</Button>
		</>
	)
}
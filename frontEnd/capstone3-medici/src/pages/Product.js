import {useState, useEffect} from 'react';

import AllProduct from '../components/AllProduct';

export default function Product() {

	const [products, setProduct] = useState([])

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/allProduct`)
		.then(res => res.json())
		.then(data => {

			console.log(data);


			setProduct(data.map(product => {
				
				return (
					
					<AllProduct key={product._id} product={product} />
				)
			}));
		})

	},[])

	return (
		<>
		{products}
		</>
	);
};
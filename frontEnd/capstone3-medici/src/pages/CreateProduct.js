import {useState, useEffect} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

import Swal from 'sweetalert2';

export default function CreateProduct() {

	const [productName, setProductName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [quantity, setQuantity] = useState('');
	const [isActive, setIsActive] = useState(false);

	function createProduct(e) {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/createProduct`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				productName: productName,
				description: description,
				price: price,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(typeof data !== "undefined") {
				Swal.fire({
					title: "Product creation successful!",
					icon: "success",
					text: "Product created!"
				});

				setProductName('');
				setDescription('');
				setPrice('');
				setQuantity('');
			} else {
				Swal.fire({
					title: "Erron on creating a product",
					icon: "error",
					text: "Try again later"
				});
			}
		})
	};

	useEffect(() => {
		if(productName !== '' && description !== '' && price !== '' && quantity !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [productName, description, price, quantity]);

	return (
		<Form onSubmit={(e) => createProduct(e)}>
			<Form.Group controlId="productName">
				<Form.Label>Product Name</Form.Label>
				<Form.Control
					type="text"
					placeholder="Enter Product Name"
					value={productName}
					onChange={e => setProductName(e.target.value)}
					required />
			</Form.Group>

			<Form.Group controlId="description">
				<Form.Label>Description</Form.Label>
				<Form.Control 
					as="textarea"
					rows={5}
					placeholder="Enter product description"
					value={description}
					onChange={e => setDescription(e.target.value)}
					required />
			</Form.Group>

			<Form.Group controlId="price">
				<Form.Label>Price: Php</Form.Label>
				<Form.Control 
					type="number"
					placeholder="Enter product price"
					value={price}
					onChange={e => setPrice(e.target.value)}
					required />
			</Form.Group>
			
			<Form.Group controlId="quantity">
				<Form.Label>Quantity</Form.Label>
				<Form.Control 
					type="number"
					placeholder="Enter product quantity"
					value={quantity}
					onChange={e => setQuantity(e.target.value)}
					required />
			</Form.Group>
			{ isActive?
			<Button variant="primary" type="submit" id="submitBtn">
				Create
			</Button>
			:
			<Button variant="danger" type="submit" id="submitBtn">
				Create
			</Button>
			}
			<Button variant="primary" type="submit" id="submitBtn" as={Link} to="/dashboard">
				Dashboard
			</Button>
		</Form>				
	)
}
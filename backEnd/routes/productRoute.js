const express = require("express");

const router = express.Router();

const productController = require("../controllers/productController");
const auth = require("../auth");

// All Products
router.get("/allProduct", (req, res) => {
	productController.allProduct().then(resultFromController => {
		res.send(resultFromController)
	});
});

// // All active products
router.get("/active", (req, res) => {
	productController.allActive().then(resultFromController => {
		res.send(resultFromController)
	})
});

// Create Product
router.post("/createProduct", (req, res) => {
	productController.createProduct(req.body).then(resultFromController => res.send(resultFromController));
});

// Retrieve a single product
router.get("/:productId", (req, res) => {
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Update a Product
router.put("/:productId", (req, res) => {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Activate or Deactivate Product
router.patch("/:productId", (req, res,) => {
	productController.changeStatusOfProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});



module.exports = router;
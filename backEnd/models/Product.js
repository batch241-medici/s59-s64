const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	productName: {
		type: String,
		required: [true, "Product name is required"]
	},
	description: {
		type: String,
		required: [true, "Product description is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	price: {
		type: Number,
		required: [true, "Product price is required"]
	},
	quantity: {
		type: Number,
		required: [true, "Product quantity is required"]
	},
	userOrders: [
	{
		userId: {
			type: String,
			required: [true, "User Id is required"]
		}
	}
	]
})

module.exports = mongoose.model("Product", productSchema);
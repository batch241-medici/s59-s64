const bcrypt = require("bcrypt");

const auth = require("../auth");

const User = require("../models/User");


// Checking if the email exists in the database
module.exports.checkEmail = (reqBody) => {

	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
};

// User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		fullName: reqBody.fullName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {
		if(error) {
			return false
		} else {
			return user
		}
	})
};

// User Login
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
};

// User Details
module.exports.getProfile = (userData) => {
	return User.findById(userData.id).then(result => {
		console.log(result);
		if(result == null) {
			return false
		} else {
			result.password ="*********";
			return result
		}
	});
};

// All Users
module.exports.allUser = () => {
	return User.find({}).then(result => {
		return result
	});
};


const User = require("../models/User");
const Order = require("../models/Order");
const Product = require("../models/Product");
const auth = require("../auth");

// Checkout
module.exports.checkout = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(result => {
		if(result) {
			return true 
		} else {
			return false
		}
	})

	let priceOfProduct = await Product.findById(data.productId).then(result => {
		return result.price
	})

	let isTotalAmount = priceOfProduct * data.quantity;

	let newOrder = new Order({
		userId: data.userId,
		products: [
		{
			productId: data.productId,
			quantity: data.quantity
		}
		],
		totalAmount: isTotalAmount
	})

	return newOrder.save().then((res, err) => {
		if(err) {
			return false
		} else {
			return true
		}
	})
};